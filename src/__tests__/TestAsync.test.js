//here used async utilites 
//here waitForElement


import React from 'react';
import { render, cleanup, fireEvent, waitForElement,waitFor,waitForElementToBeRemoved } from '@testing-library/react';
import TestAsync from '../../TestAsync'

afterEach(cleanup);
  



test('increments counter after 0.5s', async () => {
  const { getByTestId, getByText } = render(<TestAsync />); 

  fireEvent.click(getByTestId('button-up'))

  const counter = await waitForElement(() => getByText('1')) 

  expect(counter).toHaveTextContent('1')
});


/*test('movie title no longer present in DOM', async () => {
  // element is removed
  const { queryByTestId, getByText } = render(<TestAsync />); 
  const ele=await waitForElementToBeRemoved(() => queryByText('counter'));
  expect(getByText('counter')).toBeTruthy();
})*/



/*test('movie title appears', async () => {
  // element is initially not present...
  const { getByTestId, getByText } = render(<TestAsync />); 
  // wait for appearance
  await waitFor(() => {
    expect(getByText('counter')).toBeInTheDocument()
  })

  // wait for appearance and return the element
  const movie = await findByText('counter')

  expect(movie).toHaveTextContent('counter')
})*/

  
