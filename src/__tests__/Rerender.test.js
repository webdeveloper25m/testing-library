import React from 'react';
import {cleanup, fireEvent, render} from '@testing-library/react';
import NameChange from '../../Rerender'
afterEach(cleanup)
describe('Hello', () => { 
  it('should contains name with prop change', () => {
    const { getByText, rerender } = render(<NameChange name="welcome" />)
    getByText('welcome')
    rerender(<NameChange name="hello" />)
    getByText('hello')
  })
})