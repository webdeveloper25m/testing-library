import React from 'react';
import {cleanup, fireEvent, render} from '@testing-library/react';
import CheckName from '../../CheckName'
afterEach(cleanup)
describe('Hello', () => { 
  it('should contains name', () => {
    const { getByText } = render(<CheckName name="WEBDEVELOPER" />)
    getByText('WEBDEVELOPER')
  })
  
  it('should click on text', () => {
    const { getByText } = render(<CheckName name="WEBDEVELOPER" />)
    const text = getByText('WEBDEVELOPER')
    fireEvent.click(text)
  })
})
